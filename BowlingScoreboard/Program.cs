﻿using System;

namespace BowlingScoreboard
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new BowlingGame();
            var perfectPlayer = new Player( () => 10) { Name = "Master"};
            var worstPlayer = new Player( () => 0) { Name = "AbsoluteBeginner"};
            var randomPlayer = new Player( () => new Random().Next(game.GetPinsLeft() + 1))  { Name = "MrRandom"};
            game.AddPlayer(perfectPlayer);
            game.AddPlayer(worstPlayer);
            game.AddPlayer(randomPlayer);
            game.Run();
            game.Print();

        }
    }
}