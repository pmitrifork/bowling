namespace BowlingScoreboard
{
    public class Player
    {
        public Player(RollStrategy strategy)
        {
            _strategy = strategy;
        }
        
        public int Roll()
        {
            return _strategy.Invoke();
        }

        public delegate int RollStrategy();
        private readonly RollStrategy _strategy;
        public string Name { get; set; }
    }
}