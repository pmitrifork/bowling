using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BowlingScoreboard
{
    public class PlayerScoreboard
    {
        public PlayerScoreboard(int playerId, string playerName = null)
        {
            _playerId = playerId;
            _playerName = playerName ?? $"Player{playerId}";

            Reset();
        }

        public void Reset()
        {
            _currentScore = 0;
            _currentFrame = 0;
            _currentRoll = 0;
            _rollIndex = 0;
            _bonusRoll = false;
            
            for (var i = 0; i < MaxRolls; i++)
                _bonus[i] = new List<int>();

            _pins.Initialize();
        }
        
        public int GetCurrentFrame()
        {
            return _currentFrame;
        }
        
        public int GetCurrentScore()
        {
            return _currentScore;
        }
        
        public bool IsGameOver()
        {
            return _currentFrame == MaxFrames;
        }

        public void Print()
        {
            Console.WriteLine($"Scoreboard for player {_playerName}:");
            var index = 0;
            Console.WriteLine("Frame\tRoll\tPins\tTotal");
            var total = 0;
            for (var i = 0; i < _currentFrame; i++)
            {
                total += _frameBonus[i];
                for (var j = 0; j < MaxRollsInFrame; j++)
                {
                    if (_pins[i, j] != null)
                    {
                        total += _score[index];
                        Console.WriteLine($"{i+1}\t{j+1}\t{_pins[i, j]}\t{total}");
                        index++;
                    }
                }
            }
            Console.WriteLine($"TOTAL for player {_playerName}: {_currentScore}");
            Console.WriteLine();
        }

        private void NextFrame()
        {
            _currentFrame++;
            _currentRoll = 0;

        }

        private int GetRemainingPinsAfter(int roll)
        {
            var pins = 10;
            for (int i = 0; i < roll; i++)
            {
                pins -= _pins[_currentFrame, i].Value;
            }

            return pins;
        }

        public int GetRemainingPins()
        {
            return GetRemainingPinsAfter(_currentRoll);
        }
        
        public void Save(int pins)
        {
            Console.WriteLine($"Player {_playerName} rolls {pins} in frame {_currentFrame + 1} roll {_currentRoll + 1}");
            Debug.Assert(_currentFrame >= 0 && _currentFrame < MaxFrames);
            Debug.Assert(_currentRoll >= 0 && (_currentFrame < 9 && _currentRoll < 2) ||
                         (_currentFrame == 9 && _currentRoll < 3));
            _pins[_currentFrame, _currentRoll] = pins;
            if (!_bonusRoll) _score[_rollIndex] = pins;
            var scoreUpdate = 0;
            foreach (var index in _bonus[_rollIndex])
            {
                _frameBonus[index] += pins;
                scoreUpdate += pins;
            }
            // update the current score in case bonus was added (alternatively don't hold _currentScore, but calculate from array)
            _currentScore += _score[_rollIndex] + scoreUpdate;
            if (GetRemainingPinsAfter(_currentRoll + 1) > 0)
            {
                if (_currentRoll == 0) _currentRoll++;
                else if (_currentRoll == 1) NextFrame();
            }
            else
            {
                if (_currentRoll == 0) // strike, add this frame index to next two rolls
                {
                    _bonus[_rollIndex + 1].Add(_currentFrame);
                    _bonus[_rollIndex + 2].Add(_currentFrame);
                }
                else if (!_bonusRoll && _currentRoll == 1) // spare, add this frame index to next roll
                {
                    _bonus[_rollIndex + 1].Add(_currentFrame);
                }

                if (_currentFrame == 9 && _currentRoll < 2)
                {
                    _bonusRoll = true;
                    _currentRoll++; // last extra roll in last frame
                }
                else NextFrame();
            }

            _rollIndex++;
        }
        
        private const int MaxFrames = 10;
        private const int MaxRollsInFrame = 3;
        private const int MaxRolls = MaxFrames * 2 + 1;
        private int _playerId;
        private readonly string _playerName;
        private int _currentScore;
        private int _currentFrame;
        private int _currentRoll;
        private bool _bonusRoll;
        private int _rollIndex;
        private readonly int?[,] _pins = new int?[MaxFrames, MaxRollsInFrame];
        private readonly List<int>[] _bonus = new List<int>[MaxRolls];
        private readonly int[] _score = new int[MaxRolls];
        private readonly int[] _frameBonus = new int[MaxFrames];
    }
}
