using System.Collections.Generic;

namespace BowlingScoreboard
{
    public class BowlingGame
    {
        private readonly List<Player> _players = new List<Player>();
        private readonly List<PlayerScoreboard> _scoreboards = new List<PlayerScoreboard>();
        
        public int AddPlayer(Player player)
        {
            int playerIndex = _players.Count;
            _players.Add(player);
            _scoreboards.Add(new PlayerScoreboard(_players.Count, player.Name));
            return playerIndex;
        }
        
        public void Run()
        {
            while (!IsGameOver())
            {
                Next();
            }
        }

        public void ResetGame()
        {
            _currentFrame = 0;
            _currentPlayer = 0;
            foreach (var scoreboard in _scoreboards)
            {
                scoreboard.Reset();
            }
        }
        
        public int GetPlayerScore(int playerIndex) {
            var scoreboard = _scoreboards[playerIndex];
            return scoreboard.GetCurrentScore();
        }
        
        public void Roll(int pins)
        {
            var current = _scoreboards[_currentPlayer];
            current.Save(pins);
            if (current.GetCurrentFrame() > _currentFrame) NextPlayer();
        }
        
        public void Next()
        {
            var currentPlayer = _players[_currentPlayer];
            Roll(currentPlayer.Roll());
        }

        private void NextPlayer()
        {
            if (_currentPlayer < _players.Count - 1)
                _currentPlayer++;
            else
            {
                _currentFrame++;
                _currentPlayer = 0;
            }
        }

        public bool IsGameOver()
        {
            return _scoreboards[_currentPlayer].IsGameOver();
        }

        public int GetPinsLeft()
        {
            return _scoreboards[_currentPlayer].GetRemainingPins();
        }

        public void Print()
        {
            foreach (var scoreboard in _scoreboards)
            {
                scoreboard.Print();
            }
        }
        
        private int _currentFrame;
        private int _currentPlayer;
    }
}