using BowlingScoreboard;
using NUnit.Framework;

namespace UnitTests
{
    public class BasicTests
    {
        [Test]
        public void PerfectGameScoreMatches()
        {
            var game = new BowlingGame();
            var player = new Player( () => 10) { Name = "Master"};
            var playerId = game.AddPlayer(player);
            game.Run();
            Assert.AreEqual(300, game.GetPlayerScore(playerId));
        }

        [Test]
        public void SuckerGameScoreMatches()
        {
            var game = new BowlingGame();
            var player = new Player( () => 0) { Name = "Sucker"};
            var playerId = game.AddPlayer(player);
            game.Run();
            Assert.AreEqual(0, game.GetPlayerScore(playerId));
        }

        [Test]
        public void AvgStrikeGameScoreMatches()
        {
            var game = new BowlingGame();
            var player = new Player( () => 5) { Name = "AvgSparer"};
            var playerId = game.AddPlayer(player);
            game.Run();
            Assert.AreEqual(10 * (5 + 5 * 2), game.GetPlayerScore(playerId));
        }
        
        [Test]
        public void LuckyStrikeGameScoreMatches()
        {
            var game = new BowlingGame();
            var i = 0;
            var player = new Player( () => i++ % 2 == 0 ? 9 : 1) { Name = "MaxSparer"};
            var playerId = game.AddPlayer(player);
            game.Run();
            Assert.AreEqual(10 * (1 + 9 * 2), game.GetPlayerScore(playerId));
        }

        [Test]
        public void UnluckyStrikeGameScoreMatches()
        {
            var game = new BowlingGame();
            var i = 0;
            var player = new Player( () => i++ % 2 != 0 ? 9 : 1) { Name = "MinSparer"};
            var playerId = game.AddPlayer(player);
            game.Run();
            Assert.AreEqual(10 * (9 + 1 * 2), game.GetPlayerScore(playerId));
        }
    }
}